import Amplify from 'aws-amplify'
import '@aws-amplify/ui-vue'
import AwsExports from '@/aws-exports'

Amplify.configure(AwsExports)
